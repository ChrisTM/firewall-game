// the load-campaign view

var LoadView = Backbone.View.extend({
    id: 'load-view',

    template: _.template($('#load-template').html()),

    render: function () {
        this.$el.html(this.template());
        var $listArea = this.$el.find('#list-area');
        var $cList = $('<ol></ol>').appendTo($listArea);

        this.$el.find('#load-campaign-input').on('change', function (e) {
            if (e.target.files.length !== 1) { 
                return; 
            }

            var reader = new FileReader();
            reader.addEventListener('load', function (e) {
                var data = e.target.result;
                var c = new Campaign(JSON.parse(data));
                fg.g.set('campaign', c);

                fg.mainRegion.show(fg.gameView);
            });
            reader.addEventListener('error', function (data) {
                // Chrome sometimes has weirdly extra-strict local file
                // permissions. It won't let you upload a file to a page served
                // with file://. Firefox will.
                var msg = 'The file could not be uploaded.\n\nMaybe your browser\'s security policy is too strong. Try using Firefox or hosting these files on a web server.';
                alert(msg);
            });

            var file = e.target.files[0];
            reader.readAsText(file);
        });

        // because of the same origin policy, the AJAXy bits won't work right
        // if served under file://. Use something like `python -m
        // SimpleHTTPServer` to serve on localhost.
        $.getJSON('campaigns/list.json', function (campaignStubs) {
            // display 'loading...' entries for each stub
            $.each(campaignStubs, function (idx, stub) {
                $cList.append('<li><em>Downloading &ldquo;' + stub.title +
                    '&rdquo;&hellip;</em></li>');
            });

            // try to load each campaign and replace the 'loading...'.
            // in production, we might wait to load a campaign until the user
            // clicks on it.
            $.each(campaignStubs, function (idx, stub) {
                var $listEl = $cList.children().eq(idx);
                $.getJSON('campaigns/' + stub.path, function (campaignData) {
                    var $link = $('<a>')
                        .html(stub.title)
                        .attr('title', 'play this campaign')
                        .attr('href', '')
                        .click(function (e) {
                            e.preventDefault();
                            var c = new Campaign(campaignData);
                            fg.g.set('campaign', c);
                            fg.mainRegion.show(fg.gameView);
                        });
                    $listEl.html($link);
                }).error(function () {
                    $listEl.html('<em>Could not download &ldquo;' + stub.title
                        + '&rdquo;.</em>');
                    console.log('Error: Could not download the campaign at: ' + stub.path);
                });
            });
        }).error(function () { 
            $listArea.append('<p><em>Could not download the campaign list. Try uploading the campaign you want to play instead.</em></p>');
        });
    }
});
