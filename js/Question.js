var Question = Backbone.Model.extend({
    defaults: function () {
        return {
            question: '',
            choices: [],
            answerIdx: 0,
            points: 0
        };
    }
});

var Questions = Backbone.Collection.extend({
    model: Question
});
