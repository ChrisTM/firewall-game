describe('a graph', function () {
    var g;

    describe('that is empty', function () {
        beforeEach(function () {
            g = new Graph();
        });

        it('has zero nodes', function () {
            expect(g.n).toEqual(0);
        });

        it('has zero edges', function () {
            expect(g.e).toEqual(0);
        });

        // by convention/definition
        it('is not a tree', function () {
            expect(g.isTree()).toBe(false);
        });
        
        it('can have nodes added to it', function () {
            g.addNode('1.1.1.1');
            window.g = g;
            expect(g.n).toEqual(1);  
            expect(g.g['1.1.1.1']).toEqual([]);      
        });
        
        it('can remove nodes that have been added to it', function () {
            g.addNode('1.1.1.1');
            g.removeNode('1.1.1.1');
            expect(g.n).toEqual(0);
            expect(g.getNodes()).not.toContain('1.1.1.1');
            expect(g.getNodes()).toEqual([]);
        });
        
        it('can have edges added between nodes', function () {
            g.addNode('1.1.1.1');
            g.addNode('1.1.1.2');
            g.addEdge('1.1.1.1', '1.1.1.2');
            expect(g.adj('1.1.1.1')).toEqual(['1.1.1.2']);
            expect(g.adj('1.1.1.2')).toEqual(['1.1.1.1']);
            expect(g.e).toEqual(1);
        });
        
        it('can have edges between nodes removed', function () {
            g.addNode('1.1.1.1');
            g.addNode('1.1.1.2');
            g.addEdge('1.1.1.1', '1.1.1.2');
            g.removeEdge('1.1.1.1', '1.1.1.2');
            expect(g.adj('1.1.1.1')).toEqual([]);
            expect(g.adj('1.1.1.2')).toEqual([]);
            expect(g.e).toEqual(0);
        });
    });
    
   
    describe('that looks like a star', function () {
        beforeEach(function () {
            g = new Graph();
            // 3-pointed star shape
            g.addEdge('a', 'b');
            g.addEdge('a', 'c');
            g.addEdge('a', 'd');
        });

        it('keeps track of the number of nodes', function () {
            expect(g.n).toEqual(4);
        });

        it('can return a list of the nodes', function () {
            var exp = ['a', 'b', 'c', 'd'].sort();
            var obs = g.getNodes().sort();
            expect(exp).toEqual(obs);
        });

        it('keeps track of the number of edges', function () {
            expect(g.e).toEqual(3);
        });

        it('can report on a node\'s neighbors', function () {
            // we sort the results to get a canonical version (multiple
            // representations of the same set of neighbors are otherwise
            // valid).
            var observed = g.adj('a').sort();
            var expected = ['b', 'c', 'd'].sort();
            expect(observed).toEqual(expected);

            observed = g.adj('b').sort();
            expected = ['a'].sort();
            expect(observed).toEqual(expected);
        });

        it('is a tree', function () {
            expect(g.isTree()).toBe(true);
        });

        it('can find the shortest paths', function () {
            expect(g.getPath('a', 'b')).toEqual(['a', 'b']);
            expect(g.getPath('b', 'c')).toEqual(['b', 'a', 'c']);
        });

        it('can set and get positions on the nodes', function () {
            expect(function () {
                g.setPos('a', {x: 5, y: 10})
            }).not.toThrow();
            expect(g.getPos('a')).toEqual({x: 5, y: 10});
        });
    });

    describe('that looks like a triangle', function () {
        beforeEach(function () {
            g = new Graph();
            g.addEdge('a', 'b');
            g.addEdge('b', 'c');
            g.addEdge('c', 'a');
        });

        it('keeps track of the number of nodes', function () {
            expect(g.n).toEqual(3);
        });

        it('keeps track of the number of edges', function () {
            expect(g.e).toEqual(3);
        });

        it('can have nodes removed', function () {
            g.removeNode('a');
            expect(g.e).toEqual(1);
            expect(g.n).toEqual(2);
            expect(g.getNodes().sort()).toEqual(['b', 'c'].sort());
        });

        it('can report on a node\'s neighbors', function () {
            // we sort the results to get a canonical version (multiple
            // representations of the same set of neighbors are otherwise
            // valid).
            var observed = g.adj('a').sort();
            var expected = ['b', 'c'].sort();
            expect(observed).toEqual(expected);
        });

        it('is not a tree', function () {
            expect(g.isTree()).toBe(false);
        });
    });

    describe('that looks like two sticks', function () {
        beforeEach(function () {
            g = new Graph();
            g.addEdge('a', 'b');
            g.addEdge('c', 'd');
        });

        it('can report if a given edge exists or not', function () {
            expect(g.isEdge('a', 'b')).toBe(true);
            expect(g.isEdge('d', 'c')).toBe(true);
            expect(g.isEdge('a', 'c')).toBe(false);
        });

        it('is not a tree', function () {
        });
    });

    describe('that is a 3-layer balanced binary tree (from JSON obj)', function () {
        beforeEach(function () {
            g = new Graph({
                n: 7,
                e: 6,
                g: {
                    // root
                    'a': ['b', 'c'],
                    // middle layer
                    'b': ['a', 'd', 'e'],
                    'c': ['a', 'f', 'g'],
                    // leaves
                    'd': ['b'],
                    'e': ['b'],
                    'f': ['c'],
                    'g': ['c']
                }
            });
        });

        it('is a tree', function () {
            expect(g.isTree()).toBe(true);
        });

        it('has correct shortest paths', function () {
            expect(g.getPath('a', 'f')).toEqual(['a', 'c', 'f']);
            expect(g.getPath('e', 'f')).toEqual(['e', 'b', 'a', 'c', 'f']);
            expect(g.getPath('d', 'e')).toEqual(['d', 'b', 'e']);
        });
    });

    describe('that looks like a snake', function () {
        beforeEach(function () {
            g = new Graph();
            g.addEdge('a', 'b');
            g.addEdge('b', 'c');
            g.addEdge('c', 'd');
        });

        it('is a tree', function () {
            expect(g.isTree()).toBe(true);
        });

        it('has correct end-to-end shortest paths', function () {
            expect(g.getPath('a', 'd')).toEqual(['a', 'b', 'c', 'd']);
            expect(g.getPath('d', 'a')).toEqual(['d', 'c', 'b', 'a']);
        });

        it('has correct shortest paths within the middle of the graph', function () {
            expect(g.getPath('b', 'c')).toEqual(['b', 'c']);
            expect(g.getPath('d', 'b')).toEqual(['d', 'c', 'b']);
        });
    });

});
