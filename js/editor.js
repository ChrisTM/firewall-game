'use strict';

(function ($) {
    // for doing user input -> model change data binding
    // assumes form elements have the html attribute
    // `data-key` set equal to the name of the corresponding
    // model field.
    var onUserInput = function (e) {
        e.stopPropagation();
        var $field = $(e.target);
        var tag = e.target.nodeName;

        var key = $field.data('key');
        var val;

        // some fields can opt-out of this data-binding by not specifying the
        // data-key attribute.
        if (key === undefined) {
            return;
        }

        if (tag === 'INPUT') {
            var type = $field.attr('type');
            switch (type) {
                case 'checkbox':
                    val = $field.prop('checked');
                    break;
                default:
                    val = $field.val();
            }
        } else if (tag === 'TEXTAREA' || tag === 'SELECT') {
            val = $field.val();
        } else {
            throw new Error('Tried data-binding on unknown tag type.');
        }

        this.model.set(key, val);
    };

    /* Generic Views */

    window.ItemView = Backbone.View.extend({
        tagName: 'div',
        className: 'box',

        titleBarTemplate: _.template($('#item-title-bar-template').html()),

        render: function () {
            this.$el.html(this.titleBarTemplate({title: this.titleText}));
            this.$el.append(this.template(this.model.toJSON()));
            return this;
        },

        events: {
            'click .button.up': 'onMoveUp',
            'click .button.down': 'onMoveDown',
            'click .button.duplicate': 'onDuplicate',
            'click .button.remove': 'onRemove',

            'change': onUserInput,
        },

        initialize: function () {
            this.model.on('change', this.render, this);
        },

        onRemove: function (e) {
            e.stopPropagation();
            // collections automatically listen for the destroy event,
            // expecting a model as the first argument. This trigger will cause
            // any containing collections to remove this model (and in turn
            // trigger a remove event on the collection).
            this.model.trigger('destroy', this.model);
            // we use fadeTo instead of fadeOut because fadeOut sets
            // display: none when done, and we want to be able to slide it
            // up after fading.
            this.$el.fadeTo(250, 0).slideUp(250, function () {
                $(this).remove();
            });
        },

        onMoveUp: function (e) {
            e.stopPropagation();
            this.model.trigger('moveup', this.model);
        },

        onMoveDown: function (e) {
            e.stopPropagation();
            this.model.trigger('movedown', this.model);
        },

        onDuplicate: function (e) {
            e.stopPropagation();
            this.model.trigger('duplicate', this.model);
        }
    });

    /* Display a list of items.
     * Extenders of ItemListView should override:
     *   headerText -- the text in the header above the list
     *   addNewText -- the text on the UI element for adding a new item
     */
    window.ItemListView = Backbone.View.extend({
        tagName: 'section',
        headerText: 'Items',
        addNewText: 'Add new item&hellip;',
        template: _.template($('#list-template').html()),

        events: {
            'click .add': 'onAdd'
        },

        initialize: function () {
            this.collection.on('moveup', this.onMoveUp, this);
            this.collection.on('movedown', this.onMoveDown, this);
            this.collection.on('duplicate', this.onDuplicate, this);
        },

        render: function () {
            this.$el.html(this.template({
                addNewText: this.addNewText,
                headerText: this.headerText
            }));
            var $itemList = this.$el.find('.list');
            var ItemView = this.ItemView;
            this.collection.each(function (item) {
                var view = new ItemView({model: item});
                $itemList.append(view.render().el);
            });
            return this;
        },

        onAdd: function (e) {
            // prevent the creation event from bubbling up to containing
            // structures
            e.stopPropagation();
            var item = new this.collection.model();
            this.collection.add(item);

            // instead of triggering render, we'll do a fancy animation
            var view = new this.ItemView({model: item});
            var $list = this.$el.children('.list');
            view.render().$el.hide().appendTo($list).slideDown(250);
        },

        onMoveUp: function (item) {
            var idx = this.collection.indexOf(item);
            this.collection.remove(item);
            this.collection.add(item, {at: idx-1})
            this.render();
        },

        onMoveDown: function (item) {
            var idx = this.collection.indexOf(item);
            this.collection.remove(item);
            this.collection.add(item, {at: idx+1})
            this.render();
        },

        onDuplicate: function (item) {
            var idx = this.collection.indexOf(item);
            this.collection.add(item.clone(), {at: idx+1})
            this.render();
        }
    });


    /* Views */

    window.CampaignView = Backbone.View.extend({
        events: {
            'change': onUserInput
        },
        tagName: 'div',
        template: _.template($('#campaign-template').html()),
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            var challengesView = new ChallengeListView({
                collection: this.model.get('challenges')
            });
            var nodesView = new NodeListView({
                collection: this.model.get('nodes')
            });
            var graphView = new GraphView({
                nodes: this.model.get('nodes'),
                graph: this.model.get('graph')
            });
            this.$el.append(challengesView.render().el);
            this.$el.append(nodesView.render().el);
            this.$el.append(graphView.render().el);
            return this;
        }
    });

    // used by the TestPacket item view
    window.PacketView = Backbone.View.extend({
        events: {
            'change': onUserInput
        },
        template: _.template($('#packet-template').html()),
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });


    /* Item Views */

    window.ChallengeView = ItemView.extend({
        titleText: 'Challenge',
        template: _.template($('#challenge-template').html()),
        render: function () {
            ItemView.prototype.render.call(this);
            var testPacketsView = new TestPacketListView({
                collection: this.model.get('testPackets')
            });
            var questionsView = new QuestionListView({
                collection: this.model.get('questions')
            });
            this.$el.append(testPacketsView.render().el);
            this.$el.append(questionsView.render().el);
            return this;
        }
    });

    window.QuestionView = ItemView.extend({
        titleText: 'Question',
        template: _.template($('#question-template').html()),

        initialize: function () {
            // the `onUserInput` function is not appropriate for the
            // `choices` and `answerIdx` attributes. The form fields for these
            // attributes are special cased by _not_ setting the data-key
            // attribute in the template (which exempts them from
            // `onUserInput` handling) and giving them their own on-change
            // callbacks.
            this.events = _.clone(this.events); // to prevent modifying parent's events
            this.events['change li input'] = 'onChoiceChange';
            this.events['change input[type=number]'] = 'onAnswerIdxChange';
        },

        // `onChoiceChange` and `onAnswerIdxChange` assume all choices are
        // given. No special casing of empty choices.
        onChoiceChange: function (e) {
            e.stopPropagation();

            var choices = [];
            this.$el.find('ol input').each(function () {
                choices.push($(this).val());
            });

            this.model.set('choices', choices);
        },

        onAnswerIdxChange: function (e) {
            e.stopPropagation();
            this.model.set('answerIdx', Number($(e.target).val())-1);
        }
    });

    window.TestPacketView = ItemView.extend({
        titleText: 'Test Packet',
        template: _.template($('#testPacket-template').html()),
        render: function () {
            ItemView.prototype.render.call(this);
            var packetView = new PacketView({
                model: this.model.get('packet')
            });
            this.$el.children('.packet').html(
                packetView.render().el
            );
            return this;
        }
    });
    
    window.NodeView = ItemView.extend({
        titleText: 'Network Node',
        template: _.template($('#node-template').html()),
        render: function () {
            ItemView.prototype.render.call(this);
            var rulesView = new RuleListView({
                collection: this.model.get('rules')
            });
            this.$el.children('.rule-list').html(rulesView.render().el);
            return this;
        }
    });
    
    window.RuleView = ItemView.extend({
        titleText: 'Rule',
        template: _.template($('#rule-template').html()),
    });


    /* Item List Views */

    window.RuleListView = ItemListView.extend({
        ItemView: RuleView,
        headerText: 'Firewall Rules',
        addNewText: 'Add new rule&hellip;'
    });
    window.ChallengeListView = ItemListView.extend({
        ItemView: ChallengeView,
        headerText: 'Challenges',
        addNewText: 'Add new challenge&hellip;',
    });

    window.QuestionListView = ItemListView.extend({
        ItemView: QuestionView,
        headerText: 'Questions',
        addNewText: 'Add new question&hellip;',
    });

    window.TestPacketListView = ItemListView.extend({
        ItemView: TestPacketView,
        headerText: 'Test Packets',
        addNewText: 'Add new test packet&hellip;',
    });
    
    window.NodeListView = ItemListView.extend({
        ItemView: NodeView,
        headerText: 'Network Nodes',
        addNewText: 'Add a new node&hellip;'
    });

    // functions to load a different campaign into the editor
    window.load = {
        JSONObj: function (jsonObj) {
            window.c = new Campaign(jsonObj);
            window.cv = new CampaignView({model: c});
            $('#campaign-area').html(cv.render().el);
            return c;
        },

        /* Use AJAX to load a campaign into the editor.
         *
         * @params
         * campaignPath = path to a campaign .json file
         */
        URL: function (URL) {
            /* AJAX won't work with local file in Chrome
             * http://code.google.com/p/chromium/issues/detail?id=47416
             * Use a server: `python -m SimpleHTTPServer` */
            $.getJSON(URL, function (data) {
                load.JSONObj(data);
            });
        }
    };

    $(document).ready(function () {
        // start off with a blank campaign
        var c = load.JSONObj(
            { nodes: [{"name":"Fedora 17","ip":"192.168.1.100","id":"a","rules":[]},{"name":"Ubuntu 12.04","ip":"192.168.1.101","id":"b","rules":[]},{"name":"Node.js Server","ip":"192.168.1.120","id":"c","rules":[]},{"name":"MongoDB Server","ip":"192.168.1.130","id":"d","rules":[]}]
            , graph: {"n":4,"e":3,"g":{"a":["b"],"b":["a","c"],"c":["b","d"],"d":["c"]},"pos":{"a":{"x":211,"y":392},"b":{"x":245,"y":238},"c":{"x":495,"y":136},"d":{"x":459,"y":337}}}
            }
        );

        
        $('#export-button').click(function () {
            var json = JSON.stringify(c, null, '    ');
            var url = 'data:application/json,' + encodeURIComponent(json);
            var winStatus = window.open(url, 'exported-campaign');

            // if window was popup-blocked, provide data in a jQuery UI dialog
            if (winStatus === null) {
                var textArea = $('<textarea></textarea>')
                .text(json)
                .dialog({
                    title: 'Your Campaign File',
                    width: $(window).width() * 0.8,
                    height: $(window).height() * 0.8,
                    resizable: false
                })
                .css({'width': '100%', 'padding': 0, 'margin': 0});
            };
        });
    });
})(jQuery);
