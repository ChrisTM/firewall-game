var RuleList = Backbone.Collection.extend({
     model: Rule,

     initialize: function (models, options) {
        var rules = _.map(models, function (rule) {
            return (rule instanceof Rule) ? rule : new Rule(rule);
        });
        this.models = rules;

        options = options || {};
        if (options.policy === undefined) {
            this.policy = 'allow';
        } else {
            this.policy = options.policy;
        }
    },
    
    decide: function (packet) {
        var rule = _.find(this.models, function (rule) {
            return rule.match(packet);
        });
        return (rule === undefined) ? this.policy : rule.get('action');
    },
    
    addRule: function (rule) {
        if (!(rule instanceof Rule)) { 
            rule = new Rule(rule);            
        }
        this.add(rule);
    }
});
