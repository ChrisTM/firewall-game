var GameState = Backbone.Model.extend({
    defaults: function () {
        return {
            campaign: new Campaign(),
            currentChallenge: 0,
            score: 0,
            timeBonus: 0
        };
    },
});
